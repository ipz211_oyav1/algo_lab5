﻿#include <stdio.h>
#include <stdlib.h>
#include<windows.h>
#include <time.h>
#include <chrono>
#define MAX_N 100000

#define _CLIST_H
#include <stdlib.h>
#include <stdbool.h>
typedef int elemtype; // Тип элемента списку

struct elem {
	elemtype value; // Значення змінної
	struct elem* next; // Ссилка на наступний елемент
	struct elem* prev; // Ссилка на попередній елемент списку
};
struct myList {
	struct elem* head; // Перший елемент списку
	struct elem* tail; // Останнній елемент списку
	size_t size; // Кількість елементів в списку
};
typedef struct elem cNode;
typedef struct myList cList;
cList* createList(void) {
	cList* list = (cList*)malloc(sizeof(cList));
	if (list) {
		list->size = 0;
		list->head = NULL;
		list->tail = NULL;
	}
	return list;
};
bool isEmptyList(cList* list) {
	return ((list->head == NULL) || (list->tail == NULL));
}
void deleteList(cList* list) {
	cNode* head = list->head;
	cNode* next = NULL;
	while (head) {
		next = head->next;
		
			free(head);
		head = next;
	}
	free(list);
	list = NULL;
}
int pushFront(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return -1;
	}
	node->value = *data;
	node->next = list->head;
	node->prev = NULL;
	if (!isEmptyList(list)) {
		list->head->prev = node;
	}
	else {
		list->tail = node;
	}
	list->head = node;
	list->size++;
	return 0;
}
int pushBack(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return -3;
	}
	node->value = *data;
	node->next = NULL;
	node->prev = list->tail;
	if (!isEmptyList(list)) {
		list->tail->next = node;
	}
	else {
		list->head = node;
	}
	list->tail = node;
	list->size++;
	return 0;
}
int popFront(cList* list, elemtype* data) {
	cNode* node;
	if (isEmptyList(list)) {
		return -2;
	}
	node = list->head;
	list->head = list->head->next;
	if (!isEmptyList(list)) {
		list->head->prev = NULL;
	}
	else {
			list->tail = NULL;
	}
	*data = node->value;
	list->size--;
	free(node);
	return 0;
}
int popBack(cList* list, elemtype* data) {
	cNode* node = NULL;
	if (isEmptyList(list)) {
		return -2;
	}
	node = list->tail;
	list->tail = list->tail->prev;
	if (!isEmptyList(list)) {
		list->tail->next = NULL;
	}
	else {
		list->head = NULL;
	}
	*data = node->value;
	list->size--;
	free(node);
	return 0;
}
void printList(cList* list, void (*func)(elemtype*)) {
	cNode* node = list->head;
	if (isEmptyList(list)) {
		return;
	}
	while (node) {
		func(&node->value);
		node = node->next;
	}
}
cNode* getNode(cList* list, int index) {
	cNode* node = NULL;
	int i;
	if (index >= list->size) {
		return (NULL);
	}
	if (index < list->size / 2) {
		i = 0;
		node = list->head;
		while (node && i < index) {
			node = node->next;
			i++;
		}
	}
	else {
		i = list->size - 1;
			node = list->tail;
		while (node && i > index) {
			node = node->prev;
			i--;
		}
	}
	return node;
}
int pushPosition(cList* list, elemtype* data, int index) {
	if (index == 0) {
		return pushFront(list, data);
	}
	if (index == list->size) {
		return pushBack(list, data);
	}
	if (index<0 || index>list->size) {
		return -1;
	}
	cNode* next = getNode(list, index - 1)->next;
	cNode* prev = getNode(list, index)->prev;
	cNode* node = (cNode*)malloc(sizeof(cNode));
	node->value = *data;
	getNode(list, index - 1)->next = node;
	getNode(list, index)->prev = node;
	node->next = next;
	node->prev = prev;
	list->size++;
	return 0;
}
int popPosition(cList* list, int index) {
	if (index == 0) {
		elemtype tmp;
		return popFront(list, &tmp);
	}
	if (index == list->size - 1) {
		elemtype tmp;
		return popBack(list, &tmp);
	}
	if (index<0 || index>list->size - 1) {
		return -1;
	}
	cNode* next = getNode(list, index)->next;
	cNode* prev = getNode(list, index)->prev;
	free(getNode(list, index));
	getNode(list, index - 1)->next = next;
	getNode(list, index)->prev = prev;
	list->size--;
	return 0;
}
void printNode(elemtype* value) 
{
	printf("%4d ", *((int*)value));
}
void menu()
{
	system("cls");
	printf("Оберіть дію\n");
	printf("1. Сортування вибором (двохзв'язний список)\n");
	printf("2. Сортування вставками (масив)\n");
	printf("3. Сортування вставками (двохзв'язний список)\n");
	
	printf("\n>>");
}
int get_variant(int count) {
	int variant;
	scanf_s("%d", &variant);
	return variant;
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	/*cList* mylist = createList();
	cList* mylist2 = createList();*/
	int list[MAX_N], arr[MAX_N], a, b, n, c, min;
	int variant, k;
	srand(time(0));
	printf("Введіть границі: [a;b];\n a="); scanf_s("%d", &a);
	printf("b="); scanf_s("%d", &b);
	printf("Кількість елементів:"); scanf_s("%d", &n);
	do {
		menu();
		variant = get_variant(6);
		if (variant == 1) {
			cList* mylist = createList();
			auto begin = std::chrono::steady_clock::now();
			for (int i = 0; i < n; i++) {
				list[i] = a + rand() % (b - a + 1);
				pushBack(mylist, &list[i]);
			}
			printList(mylist, printNode);
			int	imin;
			for(int	i = 0; i < n - 1; i++)
			{
				imin = i;	int	min = getNode(mylist, i)->value;
				for(int j = i + 1; j < n; j++)
				{
					if(getNode(mylist, j)->value < min)
					{
						imin = j;
						min = getNode(mylist, j)->value;
					}
				}
				elemtype a = getNode(mylist, i)->value;
				elemtype b = getNode(mylist, imin)->value;
				getNode(mylist, i)->value = b;
				getNode(mylist, imin)->value = a;
			}
			printf("\n\n");
			printf("----------------------------------------------------\n");
			printList(mylist, printNode);
			deleteList(mylist);
			auto end = std::chrono::steady_clock::now();
			auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
			printf("\nThe time: %lld ms\n", elapsed_ms.count());
		}
		if (variant == 2) {
			auto begin = std::chrono::steady_clock::now();
			for (int i = 0; i < n; i++)
				arr[i] = a + rand() % (b - a + 1);
			for (int i = 0; i < n; i++)
				printf("%4d", arr[i]);
			for (int i = 1; i < n; i++)
			{
				c = arr[i];
				for (int j = i - 1; j >= 0 && arr[j] > c; j--)
				{
					arr[j + 1] = arr[j];//перебираючи масив міняємо числа по порядку вставляючи їх правильне місце
					arr[j] = c;
				}
			}
			printf("\n\n");
			printf("----------------------------------------------------\n");
			for (int i = 0; i < n; i++)
				printf("%4d", arr[i]);
			auto end = std::chrono::steady_clock::now();
			auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
			printf("\nThe time: %lld ms\n", elapsed_ms.count());
		}
		if (variant == 3) {
			cList* mylist2 = createList();
			auto begin = std::chrono::steady_clock::now();
			for (int i = 0; i < n; i++) {
				list[i] = a + rand() % (b - a + 1);
				pushBack(mylist2, &list[i]);
			}
			printList(mylist2, printNode);
			for
				(int i = 1; i < n; i++) {
				elemtype c = getNode(mylist2, i)->value;
				for(int	j = i - 1; j >= 0 && getNode(mylist2, j)->value >c; j--)
				{
					elemtype b = getNode(mylist2, j)->value;
					elemtype a = getNode(mylist2, j + 1)->value;
					getNode(mylist2, j)->value = a;
					getNode(mylist2, j + 1)->value = b;
				}
			}
			printf("\n\n");
			printf("----------------------------------------------------\n");
			printList(mylist2, printNode);
			deleteList(mylist2);
			auto end = std::chrono::steady_clock::now();
			auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
			printf("\nThe time: %lld ms\n", elapsed_ms.count());
		}
		if (variant == 4)
		{
			printf("Кількість елементів:"); scanf_s("%d", &n);
		}
		

		system("pause");
	} while (variant != 5);
}


